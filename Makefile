.DELETE_ON_ERROR:
.PHONY: all clean

FILE=main

all:
	pdflatex  --synctex=1 -file-line-error ${FILE}
	bibtex ${FILE}
	pdflatex  --synctex=1 -file-line-error ${FILE}
	pdflatex  --synctex=1 -file-line-error ${FILE}

clean:
	@rm -f *.aux *.blg *.bbl *.log *.out *.pdf *.gz



